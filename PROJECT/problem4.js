function problem4(inventory){
    let allCarYears=inventory.map(function(carDetail){
        return carDetail.car_year;
    });
    return allCarYears;

}
module.exports=problem4;