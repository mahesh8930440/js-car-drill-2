function problem5(inventory,allCarYears){
    const allOldCarYears=allCarYears.filter(function(carYear){
        if (carYear<2000){
            return carYear;
        }
    });
    
    return allOldCarYears;
}
module.exports=problem5;